package com.example.emailclient.sync;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.example.emailclient.R;
import com.example.emailclient.activities.EmailActivity;
import com.example.emailclient.activities.EmailsActivity;

public class SyncMailReceiver extends BroadcastReceiver {

    private static int notificationID = 1;
    private static String channelId = "My_Chan_Id";

    @Override
    public void onReceive(Context context, Intent ints) {

        NotificationManager nfManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);

        if (ints.getAction().equals(EmailsActivity.SYNC_DATA)) {
            int countOfMessages = ints.getIntExtra(SyncMailService.COUNT_OF_MESSAGES, -1);
            String subject = ints.getStringExtra(SyncMailService.SUBJECT);
            String content = ints.getStringExtra(SyncMailService.CONTENT);

            Intent actionIntent;
            if (countOfMessages == 1) {
                int folderId = ints.getIntExtra(SyncMailService.FOLDER_ID, -1);
                int messageId = ints.getIntExtra(SyncMailService.MESSAGE_ID, -1);
                actionIntent = new Intent(context, EmailActivity.class);
                actionIntent.putExtra("emailId", messageId);
                actionIntent.putExtra("folderId", folderId);
            } else {
                actionIntent = new Intent(context, EmailsActivity.class);
            }

            PendingIntent pIntentEmail = PendingIntent.getActivity(context, 0, actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setSmallIcon(R.drawable.ic_notif_mail);
            builder.setContentTitle(subject);
            builder.setContentText(content);
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(content));
            builder.setAutoCancel(true);
            builder.setContentIntent(pIntentEmail);

            // notificationID allows you to update the notification later on.
            nfManager.notify(notificationID, builder.build());
        }

    }

}
