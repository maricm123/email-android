package com.example.emailclient.sync;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import com.example.emailclient.activities.EmailsActivity;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.MailClientTools;

import org.jsoup.Jsoup;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncMailService extends Service {

    public static String COUNT_OF_MESSAGES = "COUNT_OF_MESSAGES";
    public static String SUBJECT = "SUBJECT";
    public static String CONTENT = "CONTENT";
    public static String FOLDER_ID = "FOLDER_ID";
    public static String MESSAGE_ID = "MESSAGE_ID";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        int status = MailClientTools.getConnectivityStatus(getApplicationContext());

        if(status == MailClientTools.TYPE_WIFI || status == MailClientTools.TYPE_MOBILE){

           //ovde cemo lepo da pozovemo retrofit i dovucemo sve nove mailove, poruke, foldere, kontakte, ali za sada samo radi probe dovlacim jednog usera
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            int userId = sharedPreferences.getInt("userId", -1);
            int accountId = sharedPreferences.getInt("accountId", -1);
            int maxId = sharedPreferences.getInt("maxId" + accountId, -1);

            Call<List<Message>> call = UtilsService.emailClientService.getNewMessages(userId, accountId, maxId);
            call.enqueue(new Callback<List<Message>>() {
                @Override
                public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                    if (response.code() == 200){
                        List<Message> messages = response.body();
                        if(!messages.isEmpty()) {
                            Intent ints = createIntentForBroadcast(messages);
                            sendBroadcast(ints);
                        }
                    } else {
                        Log.v("REZ","Response code: "+response.code());
                    }
                }

                @Override
                public void onFailure(Call<List<Message>> call, Throwable t) {
                    Log.v("REZ", t.getMessage() != null?t.getMessage():"error");
                }
            });
        }

        //PROFESOR je ovde rekao da mi treba da koristimo valjda neki: START_STICKY_WITH_INTENT, ali mi ne nudi :(
        return START_STICKY;
    }

    private Intent createIntentForBroadcast(List<Message> messages){
        Intent intent = new Intent(EmailsActivity.SYNC_DATA);
        int numOfMessages = messages.size();
        intent.putExtra(COUNT_OF_MESSAGES, numOfMessages);
        String subject = "";
        String content = "";

        // Ispis vise mailova u notifikaciji
        if(messages.size() > 1){
            subject = "You have " + numOfMessages + " new emails";
            // Posto zelimo ispisati do tri maila odbacujemo visak
            if(numOfMessages > 3){
                messages = messages.subList(0,2);
            }

            for (Message message: messages){
                content += message.getFrom() + ": " + message.getSubject() +"\n";
            }
          // Ispis jednog maila u notifikaciji
        } else {
            Message message = messages.get(0);
            subject = message.getFrom() + ": " + message.getSubject();
            content = MailClientTools.getShortString(Jsoup.parse(message.getContent()).text(), 50);
            intent.putExtra(FOLDER_ID, message.getFolderId());
            intent.putExtra(MESSAGE_ID, message.getId());
        }
        intent.putExtra(SUBJECT, subject);
        intent.putExtra(CONTENT, content);
        return intent;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
