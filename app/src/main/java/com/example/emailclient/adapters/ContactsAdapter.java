package com.example.emailclient.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emailclient.R;
import com.example.emailclient.activities.ContactsActivity;
import com.example.emailclient.model.Contact;

import java.io.IOException;
import java.util.ArrayList;

public class ContactsAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Contact> contacts;

    public ContactsAdapter(Activity activity, ArrayList<Contact> contacts) {
        this.activity = activity;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        long itemId =((Contact) getItem(position)).getId();
        return itemId;
    }

    private Bitmap fromStringToBitmap(String encodedImage){

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        Contact contact = (Contact) getItem(position);

        if (convertView == null){
            view = activity.getLayoutInflater().inflate(R.layout.contact_list_item, null);
        }

        ImageView icon = (ImageView) view.findViewById(R.id.contact_icon);
        TextView displayName = (TextView) view.findViewById(R.id.contact_displayName);
        displayName.setText(contact.getDisplayName());
        if(contact.getPhoto() == null) {
            icon.setImageResource(R.drawable.ic_contact);
        }else {
            icon.setImageBitmap(fromStringToBitmap(contact.getPhoto()));
        }
        return view;
    }

}
