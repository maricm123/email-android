package com.example.emailclient.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import org.jsoup.Jsoup;

import com.example.emailclient.R;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EmailsAdapter extends BaseAdapter implements Filterable {

    private Activity activity;
    private ArrayList<Message> displayedMessages;
    private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
    private MessageFilter filter;
    private ArrayList<Message> allMessages;

    public EmailsAdapter(Activity activity, ArrayList<Message> messages) {
        this.activity = activity;
        this.displayedMessages = messages;
        this.allMessages = messages;
    }


    @Override
    public int getCount() {
        return displayedMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return displayedMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        long itemId =((Message) getItem(position)).getId();
        return itemId;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        Message message = (Message) getItem(position);

        if(convertView==null)
            view = activity.getLayoutInflater().inflate(R.layout.message_list, null);

        TextView posiljalac = (TextView)view.findViewById(R.id.posiljalac);
        TextView naslov = (TextView)view.findViewById(R.id.naslov);
        TextView poruka = (TextView)view.findViewById(R.id.poruka);
        TextView datum = (TextView)view.findViewById(R.id.datum);
        TextView tag = (TextView)view.findViewById(R.id.tag);
        ImageView image = (ImageView)view.findViewById(R.id.item_icon);

        posiljalac.setText(message.getFrom());
        naslov.setText(message.getSubject());
        poruka.setText(Jsoup.parse(message.getContent()).text());

        Date dateTime = DateConverter.fromISO8601UTC(message.getDateTimeString());
        datum.setText(format.format(dateTime));

        posiljalac.setTag(message.getFolderId());

        ImageView img = view.findViewById(R.id.attachment);
        if (message.getAttachments().isEmpty()) {
            img.setVisibility(View.GONE);
        } else {
            img.setVisibility(View.VISIBLE);
        }

        if(message.isUnread()) {
            posiljalac.setTypeface(posiljalac.getTypeface(), Typeface.BOLD);
            naslov.setTypeface(naslov.getTypeface(), Typeface.BOLD);
            datum.setTypeface(datum.getTypeface(), Typeface.BOLD);
        }else {
            posiljalac.setTypeface(posiljalac.getTypeface(), Typeface.ITALIC);
            naslov.setTypeface(naslov.getTypeface(), Typeface.ITALIC);
            datum.setTypeface(datum.getTypeface(), Typeface.ITALIC);
        }
//        if (message.getAvatar() != -1){
//            image.setImageResource(message.getAvatar());
//        }

        return  view;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new MessageFilter();
        }
        return filter;
    }


    private class MessageFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(constraint != null && constraint.length() > 0) {
                ArrayList<Message> filteredMessages = new ArrayList<>();
                Message message = null;
                for(int i = 0; i < allMessages.size(); i++) {
                    message = allMessages.get(i);
                    if(message.getSubject().toLowerCase().contains(constraint.toString().toLowerCase())
                    || message.getContent().toLowerCase().contains(constraint.toString().toLowerCase())
                    || message.getFrom().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredMessages.add(message);
                    }
                    results.count=filteredMessages.size();
                    results.values=filteredMessages;
                }
            }else {
                results.count= allMessages.size();
                results.values= allMessages;
            }
            return  results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            displayedMessages = (ArrayList<Message>) results.values;
            notifyDataSetChanged();
        }
    }
}

