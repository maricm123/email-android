package com.example.emailclient.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.example.emailclient.BuildConfig;
import com.example.emailclient.R;
import com.example.emailclient.model.Attachment;
import com.example.emailclient.service.UtilsService;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.DOWNLOAD_SERVICE;

public class AttachmentsAdapter extends BaseAdapter {

    private Activity activity;

    private ArrayList<Attachment> attachments;

    public AttachmentsAdapter(Activity activity, ArrayList<Attachment> attachments) {
        this.activity = activity;
        this.attachments = attachments;

    }

    @Override
    public int getCount() {
        return attachments.size();
    }

    @Override
    public Object getItem(int position) {
        return attachments.get(position);
    }

    @Override
    public long getItemId(int position) {
        long itemId = ((Attachment) getItem(position)).getId();
        return itemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        final Attachment attachment = (Attachment) getItem(position);

        if (convertView == null){
            view = activity.getLayoutInflater().inflate(R.layout.attachment_list_item, null);
        }

        TextView name = (TextView) view.findViewById(R.id.attachment_name);
        ImageView downAtt = view.findViewById(R.id.download_att);
        name.setText(attachment.getName());
        name.setTag(attachment.getId());

        //klik listener na sam attachment, treba da otvori attachment
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttachment(attachment.getId());
            }
        });

        //klik listener na slicicu za download, treba da sacuva attachment na telefonu
        downAtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadAttachment(attachment.getId());
            }
        });

        return view;
    }

    private void showAttachment(int attachmentId) {

        Call<Attachment> call = UtilsService.emailClientService.getAttachment(-1, attachmentId);
        call.enqueue(new Callback<Attachment>() {
            @Override
            public void onResponse(Call<Attachment> call, Response<Attachment> response) {
                if (response.code() == 200) {
                    Attachment attac = response.body();
                    openAttachment(attac);
                } else {
                    Toast.makeText(activity, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Attachment> call, Throwable t) {
                Toast.makeText(activity, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "onFailure: nije moguce dohvatiti attachment", t);
            }
        });
    }

    private void openAttachment(Attachment attac) {
        try {
            byte[] attaData = Base64.decode(attac.getData(), Base64.DEFAULT);
            File tempFile = new File(activity.getExternalCacheDir()+"/"+attac.getName());
            FileOutputStream fos = new FileOutputStream(tempFile);//openFileOutput(attac.getName(), Context.MODE_PRIVATE);
            fos.write(attaData);
            fos.close();

            Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID+".provider", tempFile);//Uri.fromFile(tempFile);
            Intent myIntent = new Intent(Intent.ACTION_VIEW);
            myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            String mime = activity.getContentResolver().getType(uri);
            Log.e("MimeType? ", mime);
            myIntent.setDataAndType(uri, mime);
            activity.startActivity(myIntent);
        } catch (Exception e) {
            Toast.makeText(activity, "Unable to open file", Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadAttachment(int attachmentId) {

        Call<Attachment> call = UtilsService.emailClientService.getAttachment(-1, attachmentId);
        call.enqueue(new Callback<Attachment>() {
            @Override
            public void onResponse(Call<Attachment> call, Response<Attachment> response) {
                if (response.code() == 200) {
                    Attachment attac = response.body();
                    saveAttachmentToStorage(attac);
                } else {
                    Toast.makeText(activity, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Attachment> call, Throwable t) {
                Toast.makeText(activity, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "onFailure: nije moguce dohvatiti attachment", t);
            }
        });
    }

    private void saveAttachmentToStorage(Attachment attac) {
        try {
            byte[] attaData = Base64.decode(attac.getData(), Base64.DEFAULT);
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
            if(isStoragePermissionGranted()){
                File file = new File(root, attac.getName());
                if (file.exists ())
                    file.delete ();

                FileOutputStream fos = new FileOutputStream(file);
                fos.write(attaData);
                fos.flush();
                fos.close();
                DownloadManager downloadManager = (DownloadManager) activity.getSystemService(DOWNLOAD_SERVICE);

                Toast.makeText(activity, "File saved to Downloads", Toast.LENGTH_SHORT).show();
                MediaScannerConnection.scanFile(
                        activity.getApplicationContext(),
                        new String[]{file.getAbsolutePath()},
                        new String[]{attac.getMime_type()},
                        null);

            }

        } catch (Exception e) {
            Log.e("ERROR:", "problem pri downloadu", e);
            Toast.makeText(activity, "Unable to download file", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isStoragePermissionGranted() {
        String TAG = "Storage Permission";
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
}
