package com.example.emailclient.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emailclient.R;
import com.example.emailclient.model.Folder;

import java.util.ArrayList;

public class FoldersAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Folder> folders;

    public FoldersAdapter(Activity activity, ArrayList<Folder> folders) {
        this.activity = activity;
        this.folders = folders;
    }

    @Override
    public int getCount() {
        return folders.size();
    }

    @Override
    public Object getItem(int position) {
        return folders.get(position);
    }

    @Override
    public long getItemId(int position) {
        long itemId =((Folder) getItem(position)).getId();
        return itemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        Folder folder = (Folder) getItem(position);

        if (convertView == null){
            view = activity.getLayoutInflater().inflate(R.layout.folder_list_item, null);
        }

        ImageView icon = (ImageView) view.findViewById(R.id.folder_icon);
        TextView name = (TextView) view.findViewById(R.id.folder_name);
        TextView messageCount = (TextView) view.findViewById(R.id.folder_message_count);
        name.setText(folder.getName());
        messageCount.setText(String.valueOf(folder.getCountOfMessages()));

        switch (folder.getName()){
            case "Inbox":
                icon.setImageResource(R.drawable.ic_folder_inbox);
                break;
            case "Sent":
                icon.setImageResource(R.drawable.ic_folder_sent);
                break;
            case "Drafts":
                icon.setImageResource(R.drawable.ic_folder_drafts);
                break;
            case "Trash":
                icon.setImageResource(R.drawable.ic_folder_trash);
                break;
            default:
                icon.setImageResource(R.drawable.ic_drawer_folders);
        }

        return view;
    }
}
