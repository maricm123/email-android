package com.example.emailclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.emailclient.R;
import com.example.emailclient.model.NavItem;

import java.util.ArrayList;

public class DrawerListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<NavItem> navItems;
 
    public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
        this.context = context;
        this.navItems = navItems;
    }
 
    @Override
    public int getCount() {
        return navItems.size();
    }
 
    @Override
    public Object getItem(int position) {
        return navItems.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
 
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.drawer_list_item, null);
        } else {
            view = convertView;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.navitem_icon);
        TextView nameView = (TextView) view.findViewById(R.id.navitem_name);

        iconView.setImageResource(navItems.get(position).getIcon());
        nameView.setText( navItems.get(position).getName() );
 
        return view;
    }
}
