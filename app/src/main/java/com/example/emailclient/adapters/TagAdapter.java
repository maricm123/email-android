//package com.example.emailclient.adapters;
//
//import android.app.Activity;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.emailclient.R;
//import com.example.emailclient.model.Message;
//import com.example.emailclient.model.Tag;
//
//import java.util.ArrayList;
//
//public class TagAdapter extends BaseAdapter {
//    private Activity activity;
//    private ArrayList<Tag> tags;
//
//    public TagAdapter(Activity activity, ArrayList<Tag> tags) {
//        this.activity = activity;
//        this.tags = tags;
//    }
//
//    @Override
//    public int getCount() {
//        return tags.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return tags.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        long itemId =((Tag) getItem(position)).getId();
//        return itemId;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view=convertView;
//        Tag tag = (Tag) getItem(position);
//
//        if(convertView==null)
//            view = activity.getLayoutInflater().inflate(R.layout.tag_list_item, null);
//
//        TextView imeTaga = (TextView)view.findViewById(R.id.tag_name);
//
//        imeTaga.setText(tag.getName());
//        //ovo nije vidljivo na fronuendu, ovako saljem id, jer mi tamo treba za dalje koriscenje
//        imeTaga.setTag(tag.getId());
//
//
//        return view;
//    }
//}
