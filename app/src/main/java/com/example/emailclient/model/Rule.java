package com.example.emailclient.model;

import androidx.annotation.NonNull;

public class Rule {

    private Integer id;
    private String condition;
    private String value;
    private String operation;

    public Rule() {
    }

    public Rule(Integer id, String condition, String value, String operation) {
        this.id = id;
        this.condition = condition;
        this.value = value;
        this.operation = operation;
    }

    public Rule(String condition, String value, String operation) {
        this.condition = condition;
        this.value = value;
        this.operation = operation;
    }

    public Integer getId() {
        return id;
    }

    public String getCondition() {
        return condition;
    }

    public String getOperation() {
        return operation;
    }

    public String getValue() {
        return value;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "condition='" + condition + '\'' +
                ", value='" + value + '\'' +
                ", operation='" + operation + '\'' +
                '}';
    }
}
