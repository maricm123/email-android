package com.example.emailclient.model;



public class Attachment {
    private Integer id;
    private String data;
    private String mime_type;
    private String name;

    public Attachment(Integer id, String data, String mime_type, String name) {
        this.id = id;
        this.data = data;
        this.mime_type = mime_type;
        this.name = name;
    }
    public Attachment() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "id=" + id +
                ", data=" + data +
                ", mime_type='" + mime_type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
