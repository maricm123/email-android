package com.example.emailclient.model;

public class Photo {

    private Integer id;
    private String path;
    private Contact contact;

    public Photo() {
    }

    public Photo(Integer id, String path, Contact contact) {
        this.id = id;
        this.path = path;
        this.contact = contact;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
