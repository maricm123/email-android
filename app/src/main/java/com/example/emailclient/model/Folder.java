package com.example.emailclient.model;

import java.util.ArrayList;

/**
 * Created by Natasa Tutus on 5/16/2020.
 */
public class Folder {

    private Integer id;
    private String name;
    private Integer parentId;
    private int countOfMessages;
    private ArrayList<Rule> rules = new ArrayList<>();

    public Folder() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public int getCountOfMessages() {
        return countOfMessages;
    }

    public ArrayList<Rule> getRules() {
        return rules;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setCountOfMessages(int countOfMessages) {
        this.countOfMessages = countOfMessages;
    }

    public void setRules(ArrayList<Rule> rules) {
        this.rules = rules;
    }

}
