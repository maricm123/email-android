package com.example.emailclient.model;

import java.util.ArrayList;

public class Message {
    private Integer id;
    private String from;
    private String to;
    private String cc;
    private String bcc;
    private String dateTimeString;
    private String subject;
    private String content;
    private boolean unread;
    private Integer folderId;
    private ArrayList<Attachment> attachments = new ArrayList<>();
    //ovo cu posle dodati !!!
//    private Set<TagDTO> tags = new HashSet<TagDTO>();

    public Message() {
    }

    public Message(String from, String to, String cc, String bcc, String dateTimeString, String subject, String content, boolean unread, Integer folderId) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.dateTimeString = dateTimeString;
        this.subject = subject;
        this.content = content;
        this.unread = unread;
        this.folderId = folderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public ArrayList<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", dateTimeString='" + dateTimeString + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", unread=" + unread +
                ", folderId=" + folderId +
                '}';
    }
}
