package com.example.emailclient.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NavItem implements Parcelable {
    private String name;
    private int icon;
 
    public NavItem(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}
    
    @Override
    public String toString() {
    	return name;
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
        out.writeInt(icon);
	}
}
