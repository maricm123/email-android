package com.example.emailclient.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.adapters.AttachmentsAdapter;
import com.example.emailclient.model.Attachment;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.MailClientTools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailActivity extends AppCompatActivity {

    private ArrayList<Attachment> attachments = new ArrayList<>();
    private AttachmentsAdapter atachAdapter;
    private ListView attachmentsList;
    private Message message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        Toolbar toolbar = findViewById(R.id.toolbar_email);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Email");

        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        attachmentsList = findViewById(R.id.attachment_list);
        atachAdapter = new AttachmentsAdapter(EmailActivity.this, attachments);
        attachmentsList.setAdapter(atachAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.email_menuitem, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(EmailActivity.this);
            alertDialog.setMessage("Delete this message?").setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteEmailOnBackend();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
        if (id == R.id.action_forward) {
            Intent i = new Intent(EmailActivity.this, ForwardEmailActivity.class);
            i.putExtra("from", message.getFrom());
            i.putExtra("subject", message.getSubject());
            i.putExtra("content", message.getContent());
            startActivity(i);
            finish();
        }

        if (id == R.id.action_reply) {
            Intent i = new Intent(EmailActivity.this, ReplyEmailActivity.class);
            String emailAddressConverted = extractEmailAddress(message.getFrom());
            i.putExtra("from", emailAddressConverted);
            ;
            i.putExtra("subject", message.getSubject());
            startActivity(i);
            finish();
        }

        if (id == R.id.action_reply_all) {
            Toast.makeText(EmailActivity.this, "Odgovori svima", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private String extractEmailAddress(String address) {
        String[] convertFrom = address.split("<");
        if (convertFrom.length == 1) {
            return address;
        }
        String convertedFrom = convertFrom[1];
        return convertedFrom.substring(0, convertedFrom.length() - 1);
    }

    private void deleteEmailOnBackend() {
        int folderId = getIntent().getIntExtra("folderId", -1);
        int idMess = (int) getIntent().getIntExtra("emailId", -1);

        Call<Void> call = UtilsService.emailClientService.deleteMessage(folderId, idMess);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    Toast.makeText(EmailActivity.this, "Message is deleted", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(EmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(EmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "onFailure: nije moguce obrisati email", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        int id = getIntent().getIntExtra("emailId", -1);
        int folderId = getIntent().getIntExtra("folderId", -1);

        Call<Message> call = UtilsService.emailClientService.getMessage(folderId, id);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 200) {
                    message = response.body();
                    MailClientTools.storeSyncPoint(Collections.singletonList(message), EmailActivity.this);
                    TextView posiljalac = findViewById(R.id.posiljalac);
                    TextView datum = findViewById(R.id.date);
                    TextView naslov = findViewById(R.id.naslov);
                    WebView poruka = findViewById(R.id.poruka);

                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

                    posiljalac.setText(message.getFrom());
                    datum.setText(sdf.format(DateConverter.fromISO8601UTC(message.getDateTimeString())));
                    naslov.setText(message.getSubject());
                    poruka.loadData(message.getContent(), "text/html", null);
                    attachments.clear();
                    attachments.addAll(message.getAttachments());
                    atachAdapter.notifyDataSetChanged();
                    setListViewHeightBasedOnChildren(attachmentsList);
                } else {
                    Toast.makeText(EmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(EmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "onFailure: nije moguce dohvatiti email", t);
            }
        });
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
