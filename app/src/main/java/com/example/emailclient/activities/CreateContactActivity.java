package com.example.emailclient.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.emailclient.R;
import com.example.emailclient.model.Contact;
import com.example.emailclient.service.UtilsService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = findViewById(R.id.toolbar_create_contact);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateContactActivity.this, ContactsActivity.class);
                startActivity(intent);
            }
        });
        ImageButton imgButton = (ImageButton) findViewById(R.id.imgButton_createContact);
        imgButton.setImageResource(R.drawable.ic_contact);
        imgButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Toast.makeText(CreateContactActivity.this, "Postavljanje slike ce biti uskoro omoguceno", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.createcontact_menuitem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_createcontact){
            if(isValid()) {
                Contact contact = prepareContactForSaving();
                saveContactOnBackend(contact);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private boolean isValid() {
        boolean valid = true;

        EditText editFirstName = findViewById(R.id.createContact_firstName);
        EditText editLastName = findViewById(R.id.createContact_lastName);
        EditText editDisplayName = findViewById(R.id.createContact_nickName);
        EditText editEmail = findViewById(R.id.createContact_email);
        EditText editNote = findViewById(R.id.createContact_note);

        String firstName = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String displayName = editDisplayName.getText().toString();
        String email = editEmail.getText().toString();
        String note = editNote.getText().toString();

        if(TextUtils.isEmpty(firstName)) {
            editFirstName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(lastName)) {
            editLastName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(displayName)) {
            editDisplayName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(email)) {
            editEmail.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(note)) {
            editNote.setError("Missing value");
            valid = false;
        }
        return  valid;
    }

    private Contact prepareContactForSaving() {
        EditText editFirstName = findViewById(R.id.createContact_firstName);
        EditText editLastName = findViewById(R.id.createContact_lastName);
        EditText editDisplayName = findViewById(R.id.createContact_nickName);
        EditText editEmail = findViewById(R.id.createContact_email);
        EditText editNote = findViewById(R.id.createContact_note);

        String firstName = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String displayName = editDisplayName.getText().toString();
        String email = editEmail.getText().toString();
        String note = editNote.getText().toString();

        Contact contact = new Contact();
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setDisplayName(displayName);
        contact.setEmail(email);
        contact.setNote(note);

        return contact;
    }

    private void saveContactOnBackend(Contact contact) {
        SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
        int userId = preferenceManager.getInt("userId", 0);

        Call<Contact> saveContact = UtilsService.emailClientService.saveContact(contact, userId);
        saveContact.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> saveContact, Response<Contact> response) {
                if (response.code() == 201){
                    finish();
                }else {
                    Toast.makeText(CreateContactActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Toast.makeText(CreateContactActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
