package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.emailclient.R;
import com.example.emailclient.adapters.DrawerListAdapter;
import com.example.emailclient.model.Account;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.NavItem;
import com.example.emailclient.model.User;
import com.example.emailclient.service.UtilsService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private RelativeLayout drawerPane;
    private SharedPreferences sharedPreferences;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_wrapper);

        Toolbar toolbar = findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);

        ImageView imageProfile = findViewById(R.id.img_profile);
        imageProfile.setImageResource(R.drawable.ic_contact);

        fillProfileDetails();

        drawerLayout = findViewById(R.id.drawerLayout);
        drawerList = findViewById(R.id.navList);
        drawerPane = findViewById(R.id.drawerPane);

        DrawerListAdapter adapter = new DrawerListAdapter(this, getNavItems());
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawerList.setAdapter(adapter);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                 R.string.drawer_open, R.string.drawer_close){


            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }


            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
    }

    private void fillProfileDetails() {
        TextView displayName = findViewById(R.id.profile_displayname);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        displayName.setText(sharedPreferences.getString("displayName", ""));

        TextView email = findViewById(R.id.profile_email);
        email.setText(sharedPreferences.getString("accountUserName", ""));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menuitem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_logout){

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    private ArrayList<NavItem> getNavItems(){
        ArrayList<NavItem> navItems = new ArrayList<>();
        navItems.add(new NavItem(getString(R.string.emails), R.drawable.ic_drawer_mails));
        navItems.add(new NavItem(getString(R.string.contacts), R.drawable.ic_drawer_contacts));
        navItems.add(new NavItem(getString(R.string.folders), R.drawable.ic_drawer_folders));
        navItems.add(new NavItem(getString(R.string.settings), R.drawable.ic_drawer_settings));
        navItems.add(new NavItem(getString(R.string.logout), R.drawable.ic_drawer_logout));
        return navItems;
    }



    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        if(position == 0){
            Intent intent = new Intent(ProfileActivity.this, EmailsActivity.class);
            startActivity(intent);
        }else if(position == 1){
            Intent intent = new Intent(ProfileActivity.this, ContactsActivity.class);
            startActivity(intent);
        }else if(position == 2){
            Intent intent = new Intent(ProfileActivity.this, FoldersActivity.class);
            startActivity(intent);
        }else if(position == 3){
            Intent intent = new Intent(ProfileActivity.this, SettingsActivity.class);
            startActivity(intent);
        }else if(position == 4){
            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
            startActivity(intent);
        }else{
            Log.e("DRAWER", "Nesto van opsega!");
        }

        drawerList.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerPane);
    }

    @Override
    protected void onResume() {
        super.onResume();

        int userId = sharedPreferences.getInt("userId", -1);
        Call<User> call = UtilsService.emailClientService.getUserById(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 200){
                    user = response.body();
                    //uzimam userovu listu accounta i dajem metodi koja setuje vrednosti spineru
                    setAccountSpinner(user.getAccounts());
                }else{
                    Toast.makeText(ProfileActivity.this, "Problem sa serverom" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(ProfileActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce pronaci user-a", t);
            }
        });
    }

    private void setAccountSpinner(ArrayList<Account> accounts){

        Spinner spinner = (Spinner) findViewById(R.id.spinner_accounts);
        ArrayAdapter<Account> adapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                accounts);

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        //na ovaj nacin spineru kazem da selektuje account sa kojim smo trenutno ulogovani
        final String accountName = sharedPreferences.getString("accountUserName", "");
        int position = adapter.getPosition(getAccountByName(accounts, accountName));
        spinner.setSelection(position);

        //postavljam listener na spiner, kako bih mogla setovati nova podesavanja ako se izabere drugi account
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Account account = (Account) parent.getItemAtPosition(position);
                //vrsim setovanje podataka u shared preference -u (ubacujem podatke od izabranog accounta)
                changeUserData(account);
                //popunjavam podatke za prikaz detalja na profilu
                fillProfileDetails();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //metod koji setuje nove podatke u shared preference
    private void changeUserData(Account account) {

        //ovde prvo uzmem editor, preko njega izmenim sve podatke, pa onda uradim na kraju "apply()"
        // da ne bih morala na svakoj liniji raditi "apply()", ovako je bolje
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("accountId", account.getId());
        editor.putString("accountUserName", account.getUsername());
        editor.putString("displayName", account.getDisplayName());

        for (Folder folder: account.getFolders()) {
            editor.putInt(folder.getName(), folder.getId()).apply();
        }
        editor.apply();
    }

    private Account getAccountByName(List<Account> accounts, String accountName) {
        for (Account account: accounts) {
            if(accountName.equals(account.getUsername())){
                return account;
            }
        }
        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
