package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;

import com.example.emailclient.R;
import com.example.emailclient.adapters.ContactsAdapter;
import com.example.emailclient.adapters.DrawerListAdapter;
import com.example.emailclient.model.Contact;
import com.example.emailclient.model.NavItem;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.ProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ListView contactList;
    private ActionBarDrawerToggle drawerToggle;
    private RelativeLayout drawerPane;
    private ArrayList<Contact> contacts = new ArrayList<>();
    private ContactsAdapter cAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_wrapper);

        Toolbar toolbar = findViewById(R.id.toolbar_contacts);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactsActivity.this, CreateContactActivity.class);
                startActivity(intent);
            }
        });

        contactList = findViewById(R.id.contact_list);
        cAdapter = new ContactsAdapter(ContactsActivity.this, contacts);
        contactList.setOnItemClickListener(new ContactClickListener());
        contactList.setAdapter(cAdapter);

        drawerLayout = findViewById(R.id.drawerLayout);
        drawerList = findViewById(R.id.navList);
        drawerPane = findViewById(R.id.drawerPane);

        DrawerListAdapter adapter = new DrawerListAdapter(this, getNavItems());
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawerList.setAdapter(adapter);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.profileBox);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
    }


    private ArrayList<NavItem> getNavItems(){
        ArrayList<NavItem> navItems = new ArrayList<>();
        navItems.add(new NavItem(getString(R.string.emails), R.drawable.ic_drawer_mails));
        navItems.add(new NavItem(getString(R.string.contacts), R.drawable.ic_drawer_contacts));
        navItems.add(new NavItem(getString(R.string.folders), R.drawable.ic_drawer_folders));
        navItems.add(new NavItem(getString(R.string.settings), R.drawable.ic_drawer_settings));
        navItems.add(new NavItem(getString(R.string.logout), R.drawable.ic_drawer_logout));
        return navItems;
    }

    private class ContactClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(ContactsActivity.this, ContactActivity.class);
            intent.putExtra("contactId", id);
            startActivity(intent);
        }
    }



    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        if(position == 0){
            Intent intent = new Intent(ContactsActivity.this, EmailsActivity.class);
            startActivity(intent);
        }else if(position == 1){
            Intent intent = new Intent(ContactsActivity.this, ContactsActivity.class);
            startActivity(intent);
        }else if(position == 2){
            Intent intent = new Intent(ContactsActivity.this, FoldersActivity.class);
            startActivity(intent);
        }else if(position == 3){
            Intent intent = new Intent(ContactsActivity.this, SettingsActivity.class);
            startActivity(intent);
        }else if(position == 4){

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(ContactsActivity.this, LoginActivity.class);
            startActivity(intent);
        }else{
            Log.e("DRAWER", "Nesto van opsega!");
        }

        drawerList.setItemChecked(position, true);
//        if(position != 5) // za sve osim za sync
//        {
//            setTitle(mNavItems.get(position).getmTitle());
//        }
        drawerLayout.closeDrawer(drawerPane);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final ProgressDialog progressDialog = new ProgressDialog(ContactsActivity.this,"Please wait...");
        progressDialog.showDialog();
        SharedPreferences sharedPreferences =  PreferenceManager.getDefaultSharedPreferences(this);
        int userId = sharedPreferences.getInt("userId",0);

        Call<List<Contact>> getContacts = UtilsService.emailClientService.getContactsByUser(userId);
        getContacts.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                if(response.code() == 200) {
                    contacts.clear();
                    contacts.addAll(response.body());
                    cAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(ContactsActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Toast.makeText(ContactsActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                progressDialog.dismissDialog();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
