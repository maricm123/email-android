package com.example.emailclient.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.emailclient.R;
import com.example.emailclient.model.Contact;
import com.example.emailclient.service.UtilsService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    private Integer userId;
    private Integer contactId;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar_contact);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_menuitem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_savecontact){
            if(isValid()) {
                Contact contact = prepareContactForUpdate();
                updateContactOnBackend(contact);
            }
        }
        if(item.getItemId() == R.id.action_delete_contact){
            contactId = (int) getIntent().getLongExtra("contactId", -1);
            deleteContactOnBackend(contactId);
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValid() {
        boolean valid = true;

        EditText editFirstName = findViewById(R.id.contact_firstName);
        EditText editLastName = findViewById(R.id.contact_lastName);
        EditText editDisplayName = findViewById(R.id.contact_nickName);
        EditText editEmail = findViewById(R.id.contact_email);
        EditText editNote = findViewById(R.id.contact_note);

        String firstName = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String displayName = editDisplayName.getText().toString();
        String email = editEmail.getText().toString();
        String note = editNote.getText().toString();

        if(TextUtils.isEmpty(firstName)) {
            editFirstName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(lastName)) {
            editLastName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(displayName)) {
            editDisplayName.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(email)) {
            editEmail.setError("Missing value");
            valid = false;
        }
        if(TextUtils.isEmpty(note)) {
            editNote.setError("Missing value");
            valid = false;
        }
        return  valid;
    }

    private Contact prepareContactForUpdate() {

        contactId = (int) getIntent().getLongExtra("contactId", -1);

        EditText editFirstName = findViewById(R.id.contact_firstName);
        EditText editLastName = findViewById(R.id.contact_lastName);
        EditText editDisplayName = findViewById(R.id.contact_nickName);
        EditText editEmail = findViewById(R.id.contact_email);
        EditText editNote = findViewById(R.id.contact_note);

        String firstName = editFirstName.getText().toString();
        String lastName = editLastName.getText().toString();
        String displayName = editDisplayName.getText().toString();
        String email = editEmail.getText().toString();
        String note = editNote.getText().toString();

        Contact contact = new Contact();
        contact.setId(contactId);
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setDisplayName(displayName);
        contact.setEmail(email);
        contact.setNote(note);

        return contact;
    }

    private void updateContactOnBackend(Contact contact) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getInt("userId", 0);
        contactId = (int) getIntent().getLongExtra("contactId", -1);

        Call<Contact> updateContact = UtilsService.emailClientService.updateContact(contact, userId, contactId);
        updateContact.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> updateContact, Response<Contact> response) {
                if (response.code() == 200){
                    finish();
                }else {
                    Toast.makeText(ContactActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Toast.makeText(ContactActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void deleteContactOnBackend(int contactId) {
        sharedPreferences =  PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getInt("userId",0);
        Call<Void> deleteContact = UtilsService.emailClientService.deleteContact(userId, contactId);
        deleteContact.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    finish();
                } else {
                    Toast.makeText(ContactActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(ContactActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Bitmap fromStringToBitmap(String encodedImage){

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPreferences =  PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getInt("userId",0);
        contactId = (int) getIntent().getLongExtra("contactId", -1);

        Call<Contact> getContact = UtilsService.emailClientService.getContact(userId, contactId);
        getContact.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> getContact, Response<Contact> response) {
                if (response.code() == 200){

                    Contact contact = response.body();
                    ImageView image = findViewById(R.id.img_contact);
                    EditText firstName = findViewById(R.id.contact_firstName);
                    EditText lastName = findViewById(R.id.contact_lastName);
                    EditText displayName = findViewById(R.id.contact_nickName);
                    EditText email = findViewById(R.id.contact_email);
                    EditText note = findViewById(R.id.contact_note);

                    if(contact.getPhoto() == null) {
                        image.setImageResource(R.drawable.ic_contact);
                    }else {
                        image.setImageBitmap(fromStringToBitmap(contact.getPhoto()));
                    }
                    firstName.setText(contact.getFirstName());
                    lastName.setText(contact.getLastName());
                    displayName.setText(contact.getDisplayName());
                    email.setText(contact.getEmail());
                    note.setText(contact.getNote());
                }else {
                    Toast.makeText(ContactActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Toast.makeText(ContactActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
            }
        });
    }




    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
