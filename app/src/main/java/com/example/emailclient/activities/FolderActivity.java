package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.adapters.EmailsAdapter;
import com.example.emailclient.adapters.FoldersAdapter;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.MailClientTools;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FolderActivity extends AppCompatActivity {

    private Integer folderId;
    private Integer parentId;
    private String folderName;
    private FoldersAdapter subfoldersAdapter;
    private EmailsAdapter emailsAdapter;
    private ArrayList<Folder> subfolders = new ArrayList<>();
    private ArrayList<Message> messagesByFolder = new ArrayList<>();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_folder);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        folderId = (int) getIntent().getLongExtra("folderId", -1);
        parentId = getIntent().getIntExtra("parentId", -1);
        folderName = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(folderName);

        FloatingActionButton fab = findViewById(R.id.fab);
        if (isDefaultFolder()){
            fab.setVisibility(View.GONE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FolderActivity.this, CreateFolderActivity.class);
                intent.putExtra("parentId", folderId);
                startActivity(intent);
            }
        });

        ListView subfoldersList = findViewById(R.id.subfolder_list);
        subfoldersAdapter = new FoldersAdapter(FolderActivity.this, subfolders);
        subfoldersList.setOnItemClickListener(new FolderClickListener());
        subfoldersList.setAdapter(subfoldersAdapter);

        ListView mailsList = findViewById(R.id.mail_list_by_folder);
        emailsAdapter = new EmailsAdapter(FolderActivity.this, messagesByFolder);
        mailsList.setOnItemClickListener(new FolderActivity.EmailClickListener());
        mailsList.setAdapter(emailsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.folder_manuitems, menu);
        checkFolderPermission(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_edit){
            Intent intent = new Intent(FolderActivity.this, EditFolderActivity.class);
            intent.putExtra("folderId", folderId);
            intent.putExtra("parentId", parentId);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.action_deleteF){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            int accountId = sharedPreferences.getInt("accountId", -1);
            deleteFolderOnBackend(accountId);
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkFolderPermission(Menu menu){
        if (isDefaultFolder()){
            MenuItem delete = menu.findItem(R.id.action_deleteF);
            MenuItem edit = menu.findItem(R.id.action_edit);
            delete.setVisible(false);
            edit.setVisible(false);
            invalidateOptionsMenu();
        }
    }

    private boolean isDefaultFolder(){
        String folderName = getIntent().getStringExtra("name");
        return ("Inbox").equals(folderName) || ("Sent").equals(folderName) || ("Drafts").equals(folderName) || ("Trash").equals(folderName);
    }

    private class FolderClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView name = view.findViewById(R.id.folder_name);
            Intent intent = new Intent(FolderActivity.this, FolderActivity.class);
            intent.putExtra("folderId", id);
            intent.putExtra("parentId", folderId);
            intent.putExtra("name", name.getText());
            startActivity(intent);
        }
    }

    private class EmailClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(("Drafts").equals(folderName)) {
                Intent intent = new Intent(FolderActivity.this, EditEmailActivity.class);
                intent.putExtra("emailId", (int)id);
                startActivity(intent);
            }else {
                Intent intent = new Intent(FolderActivity.this, EmailActivity.class);
                intent.putExtra("emailId", (int)id);
                startActivity(intent);
            }
        }
    }

    private void deleteFolderOnBackend(int accountId){
        Call<Void> call = UtilsService.emailClientService.deleteFolder(accountId, folderId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200){
                    finish();
                }else{
                    Toast.makeText(FolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(FolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce obrisati folder", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int accountId = sharedPreferences.getInt("accountId", -1);

        Call <List<Folder>> call = UtilsService.emailClientService.getSubfolders(accountId, folderId);
        call.enqueue(new Callback<List<Folder>>() {
            @Override
            public void onResponse(Call<List<Folder>> call, Response<List<Folder>> response) {
                if (response.code() == 200){
                    subfolders.clear();
                    subfolders.addAll(response.body());
                    subfoldersAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(FolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Folder>> call, Throwable t) {
                Toast.makeText(FolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce preuzeti subfoldere", t);
            }
        });

        Call<List<Message>> call1 = UtilsService.emailClientService.getMessagesByFolder(folderId);
        call1.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if (response.code() == 200){
                    List<Message> messages = response.body();
                    MailClientTools.sortirajPoruke(messages, FolderActivity.this);

                    // zelim da "proverim" samo Inbox i custom Foldere
                    String folderName = getIntent().getStringExtra("name");
                    if (!("Sent".equals(folderName) || "Drafts".equals(folderName) || "Trash".equals(folderName))){
                        MailClientTools.storeSyncPoint(messages, FolderActivity.this);
                    }

                    messagesByFolder.clear();
                    messagesByFolder.addAll(messages);
                    emailsAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(FolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                Toast.makeText(FolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce preuzeti mailove po folderu", t);
            }
        });

    }
}
