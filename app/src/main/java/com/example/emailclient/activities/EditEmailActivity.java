package com.example.emailclient.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;
import com.example.emailclient.service.UtilsService;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditEmailActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private Message message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email);
        Toolbar toolbar = findViewById(R.id.toolbar_edit_email);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditEmailActivity.this);
                alertDialog.setMessage("Delete this message?").setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteEmailOnBackend();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });
            AlertDialog alert = alertDialog.create();
            alert.show();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String emailAcc = sharedPreferences.getString("accountUserName", "");
        String displayNameAcc = sharedPreferences.getString("displayName", "");
        String fromText = displayNameAcc + "  <" + emailAcc + ">";
        TextView textView = findViewById(R.id.novi_email_posiljalacD);
        textView.setText(fromText);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createmail_menuitem, menu);
        ifEditEmail(menu);
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_send) {
            if(isValid()) {
                Message message = prepareMessageToSend();
                sendMessage(message);
            }
        }
        return super.onOptionsItemSelected(item);
    }




    private boolean isValid(){
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalacD);
        String to_address = to_addressEdit.getText().toString().trim();
        if (TextUtils.isEmpty(to_address)) {
            to_addressEdit.setError("Receiver is required");
//            to_addressEdit.requestFocus();
            valid = false;
        }

        EditText subjectEdit = findViewById(R.id.novi_email_porukaD);
        String subject = subjectEdit.getText().toString().trim();
        if (TextUtils.isEmpty(subject)) {
            subjectEdit.setError("Subject is required");
            valid = false;
        }

        EditText contentEdit = findViewById(R.id.novi_email_naslovD);
        String content = contentEdit.getText().toString().trim();

        if (TextUtils.isEmpty(content)) {
            contentEdit.setError("Content is required");
            valid = false;
        }

        return valid;
    }

    private Message prepareMessageToSend() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");
        message.setFrom(senderMail);

        EditText to_addressEdit = findViewById(R.id.novi_email_primalacD);
        String to_address = to_addressEdit.getText().toString();
        message.setTo(to_address);

        EditText ccEdit = findViewById(R.id.novi_email_ccD);
        String cc = ccEdit.getText().toString();
        message.setCc(cc);

        EditText bccEdit = findViewById(R.id.novi_email_bccD);
        String bcc = bccEdit.getText().toString();
        message.setBcc(bcc);

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);
        message.setDateTimeString(dateTimeString);

        EditText subjectEdit = findViewById(R.id.novi_email_naslovD);
        String subject = subjectEdit.getText().toString().trim();
        message.setSubject(subject);

        EditText contentEdit = findViewById(R.id.novi_email_porukaD);
        String content = contentEdit.getText().toString().trim();
        message.setContent(content);

        Integer folderId = sharedPreferences.getInt("Sent", -1);
        message.setFolderId(folderId);

        return message;
    }

    private void sendMessage(Message message) {

        Integer folderId = sharedPreferences.getInt("Sent", -1);
        int idMessage = getIntent().getIntExtra("emailId", -1);

        Call<Message> call = UtilsService.emailClientService.updateMessage(message, folderId, idMessage);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 200){
                    Toast.makeText(EditEmailActivity.this, "Poruka je poslata", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(EditEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(EditEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    private void deleteEmailOnBackend(){
        int folderId = getIntent().getIntExtra("folderId", -1);
        int idMess = (int) getIntent().getIntExtra("emailId", -1);

        Call<Void> call = UtilsService.emailClientService.deleteMessage(folderId, idMess);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    Toast.makeText(EditEmailActivity.this, "Message is deleted", Toast.LENGTH_SHORT).show();
                    finish();
                }else {
                    Toast.makeText(EditEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(EditEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "onFailure: nije moguce obrisati email", t);
            }
        });
    }

    private void ifEditEmail(Menu menu){
            MenuItem close = menu.findItem(R.id.action_close_create);
            close.setVisible(false);
//            invalidateOptionsMenu();
        }
    @Override
    protected void onResume() {
        super.onResume();

        int idMessage = getIntent().getIntExtra("emailId", -1);
        int folderId = sharedPreferences.getInt("Drafts", -1);

        Call<Message> call = UtilsService.emailClientService.getMessage(folderId, idMessage);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 200){
                    message = response.body();

                    EditText to = findViewById(R.id.novi_email_primalacD);
                    EditText cc = findViewById(R.id.novi_email_ccD);
                    EditText bcc = findViewById(R.id.novi_email_bccD);
                    EditText subject = findViewById(R.id.novi_email_naslovD);
                    EditText content = findViewById(R.id.novi_email_porukaD);

                    to.setText(message.getTo());
                    cc.setText(message.getCc());
                    bcc.setText(message.getBcc());
                    subject.setText(message.getSubject());
                    content.setText(message.getContent());
                } else {
                    Toast.makeText(EditEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(EditEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });

    }




    @Override
    protected void onPause() {
        super.onPause();
//        Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStop() {
        super.onStop();
//        Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStart() {
        super.onStart();
//        Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }
}