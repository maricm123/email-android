package com.example.emailclient.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.emailclient.R;
import com.example.emailclient.adapters.DrawerListAdapter;
import com.example.emailclient.adapters.EmailsAdapter;
import com.example.emailclient.model.Message;
import com.example.emailclient.model.NavItem;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.sync.SyncMailReceiver;
import com.example.emailclient.sync.SyncMailService;
import com.example.emailclient.tools.MailClientTools;
import com.example.emailclient.tools.ProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private RelativeLayout drawerPane;
    private SharedPreferences sharedPreferences;
    private PendingIntent pendingIntent;
    private AlarmManager manager;
    public static String SYNC_DATA = "SYNC_DATA";
    private SyncMailReceiver syncReceiver;
    private boolean allowSync;
    private String syncTime;
    private String sortOrder;
    private ArrayList<Message> messages = new ArrayList<>();
    private EmailsAdapter eAdapter;
    private Message message;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emails_wrapper);


        Toolbar toolbar = findViewById(R.id.toolbar_emails);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmailsActivity.this, CreateEmailActivity.class);
                startActivity(intent);

            }
        });

        ListView emailList = findViewById(R.id.email_list);
        eAdapter = new EmailsAdapter(EmailsActivity.this, messages);
        emailList.setOnItemClickListener(new EmailClickListener());
        emailList.setAdapter(eAdapter);

        drawerLayout = findViewById(R.id.drawerLayout);
        drawerList = findViewById(R.id.navList);
        drawerPane = findViewById(R.id.drawerPane);

        DrawerListAdapter adapter = new DrawerListAdapter(this, getNavItems());
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawerList.setAdapter(adapter);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.profileBox);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailsActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        setUpReceiver();
        consultPreferences();

//        Intent intent = getIntent();
//        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
//            String query = intent.getStringExtra(SearchManager.QUERY);
//        }


    }

    private void setUpReceiver(){
        syncReceiver = new SyncMailReceiver();

        Intent alarmIntent = new Intent(this, SyncMailService.class);
        pendingIntent = PendingIntent.getService(this, 0, alarmIntent, 0);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
    }

    private void consultPreferences() {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        allowSync = sharedPreferences.getBoolean(getString(R.string.pref_sync), false);
        syncTime = sharedPreferences.getString(getString(R.string.refresh_preference), "1");
        sortOrder = sharedPreferences.getString(getString(R.string.sort_preference), "ASC");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.emails_menuitem, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                eAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }



    private ArrayList<NavItem> getNavItems() {
        ArrayList<NavItem> navItems = new ArrayList<>();
        navItems.add(new NavItem(getString(R.string.emails), R.drawable.ic_drawer_mails));
        navItems.add(new NavItem(getString(R.string.contacts), R.drawable.ic_drawer_contacts));
        navItems.add(new NavItem(getString(R.string.folders), R.drawable.ic_drawer_folders));
        navItems.add(new NavItem(getString(R.string.settings), R.drawable.ic_drawer_settings));
        navItems.add(new NavItem(getString(R.string.logout), R.drawable.ic_drawer_logout));
        return navItems;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private class EmailClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            TextView posiljalac = (TextView)view.findViewById(R.id.posiljalac);
            Integer folderId = (Integer) posiljalac.getTag();

            Intent intent = new Intent(EmailsActivity.this, EmailActivity.class);
            intent.putExtra("emailId", (int)id);
            intent.putExtra("folderId", folderId);
            startActivity(intent);
        }
    }

    private void selectItemFromDrawer(int position) {
        if (position == 0) {
            Intent intent = new Intent(EmailsActivity.this, EmailsActivity.class);
            startActivity(intent);
        } else if (position == 1) {
            Intent intent = new Intent(EmailsActivity.this, ContactsActivity.class);
            startActivity(intent);
        } else if (position == 2) {
            Intent intent = new Intent(EmailsActivity.this, FoldersActivity.class);
            startActivity(intent);
        } else if (position == 3) {
            Intent intent = new Intent(EmailsActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else if (position == 4) {

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(EmailsActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            Log.e("DRAWER", "Nesto van opsega!");
        }

        drawerList.setItemChecked(position, true);
//        if(position != 5) // za sve osim za sync
//        {
//            setTitle(mNavItems.get(position).getmTitle());
//        }
        drawerLayout.closeDrawer(drawerPane);
    }


    @Override
    protected void onResume() {
        super.onResume();
        final ProgressDialog progressDialog = new ProgressDialog(EmailsActivity.this,"Please wait...");
        progressDialog.showDialog();

        consultPreferences();

        if (manager == null) {
            setUpReceiver();
        }

        if(allowSync){
            int interval = MailClientTools.calculateTimeTillNextSync(Integer.parseInt(syncTime));
            manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
            Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(SYNC_DATA);
        registerReceiver(syncReceiver, filter);


        int folderId = sharedPreferences.getInt("Inbox", 0);
        Call<List<Message>> call = UtilsService.emailClientService.getMessagesByFolder(folderId);
        call.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if (response.code() == 200){

                    //sortiranje mailova po rastucem redosledu ASC
                    List<Message> listaM = response.body();
                    MailClientTools.storeSyncPoint(listaM, EmailsActivity.this);
                    MailClientTools.sortirajPoruke(listaM, EmailsActivity.this);

                    messages.clear();
                    messages.addAll(listaM);
                    eAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(EmailsActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                Toast.makeText(EmailsActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce preuzeti inbox mailove", t);
                progressDialog.dismissDialog();
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        //nista ne radi
    }

}
