package com.example.emailclient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.Rule;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.RuleCustomView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFolderActivity extends AppCompatActivity {

    private Integer folderId;
    private int accountId;
    private LinearLayout rulesList;
    private Folder folder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_create_folder);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        accountId = sharedPreferences.getInt("accountId", -1);
        folderId = getIntent().getIntExtra("folderId", -1);

        Call<Folder> call = UtilsService.emailClientService.getFolderById(accountId, folderId);
        call.enqueue(new Callback<Folder>() {
            @Override
            public void onResponse(Call<Folder> call, Response<Folder> response) {
                if (response.code() == 200){
                    folder = response.body();
                    EditText fieldFolderName = findViewById(R.id.edit_folder_name);
                    fieldFolderName.setText(folder.getName());
                    showRulesPerFolder(folder);
                }else{
                    Toast.makeText(EditFolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Folder> call, Throwable t) {
                Toast.makeText(EditFolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce pronaci folder", t);
            }
        });

        Button btnRule = (Button) findViewById(R.id.dodaj_rule);
        btnRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RuleCustomView viewRule = new RuleCustomView(getApplicationContext());
                rulesList.addView(viewRule);
            }
        });

    }

    private void showRulesPerFolder(Folder folder){
        rulesList = findViewById(R.id.rules_list);
        for (Rule r:folder.getRules()) {
            RuleCustomView customView = new RuleCustomView(getApplicationContext());
            Spinner condition = customView.findViewById(R.id.spinner_condition);
            EditText editValue = customView.findViewById(R.id.edit_rule_value);
            Spinner operation = customView.findViewById(R.id.spinner_operation);

//            ArrayAdapter<String> adapter = (ArrayAdapter<String>)condition.getAdapter();
//            int position = adapter.getPosition(r.getCondition());
//            condition.setSelection(position);

            condition.setSelection(((ArrayAdapter<String>)condition.getAdapter()).getPosition(r.getCondition()));
            editValue.setText(r.getValue());
            editValue.setTag(r.getId());
            operation.setSelection(((ArrayAdapter<String>)operation.getAdapter()).getPosition(r.getOperation()));

            rulesList.addView(customView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.createfolder_manuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_savefolder){

            if (isValid()) {
                Folder folder = prepareFolderForUpdate();
                updateFolderOnBackend(folder, accountId, folderId);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValid(){
        boolean valid = true;

        EditText editFolderName = findViewById(R.id.edit_folder_name);
        String fName = editFolderName.getText().toString();
        if (("Inbox").equalsIgnoreCase(fName) || ("Sent").equalsIgnoreCase(fName)
                || ("Drafts").equalsIgnoreCase(fName)|| ("Trash").equalsIgnoreCase(fName)){
            editFolderName.setError("Reserved name");
            valid = false;
        }else if (TextUtils.isEmpty(fName)){
            editFolderName.setError("Missing value");
            valid = false;
        }

        int rulesCount = rulesList.getChildCount();
        for (int i = 0; i < rulesCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) rulesList.getChildAt(i);
            EditText editValue = relativeLayout.findViewById(R.id.edit_rule_value);
            String value = editValue.getText().toString();
            if(TextUtils.isEmpty(value)) {
                editValue.setError("Missing value");
                valid = false;
            }
        }
        return valid;
    }

    private Folder prepareFolderForUpdate(){

        EditText editFolderName = findViewById(R.id.edit_folder_name);
        String folderName = editFolderName.getText().toString();

        ArrayList<Rule> rules = extractRulesFromDinamicLayout();

        Folder folder = new Folder();
        folder.setId(folderId);
        folder.setName(folderName);
        int parentId = getIntent().getIntExtra("parentId", -1);
        if (parentId != -1){
            folder.setParentId(parentId);
        }
        folder.setRules(rules);
        return folder;
    }

    private ArrayList<Rule> extractRulesFromDinamicLayout ( ){

        int rulesCount = rulesList.getChildCount();
        ArrayList<Rule> rules = new ArrayList<>();
        for (int i = 0; i < rulesCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) rulesList.getChildAt(i);

            Spinner spCondView = relativeLayout.findViewById(R.id.spinner_condition);
            String condition = spCondView.getSelectedItem().toString();

            EditText editValue = relativeLayout.findViewById(R.id.edit_rule_value);
            String value = editValue.getText().toString();
            Integer idRule =(Integer) editValue.getTag();

            Spinner spOperView = relativeLayout.findViewById(R.id.spinner_operation);
            String operation = spOperView.getSelectedItem().toString();

            Rule rule = new Rule(condition, value, operation);
            if (idRule!=null){
                rule.setId(idRule);
            }
            rules.add(rule);
        }
        return rules;
    }

    private void updateFolderOnBackend(Folder folder, int accountId, Integer folderId ){
        Call<Folder> call = UtilsService.emailClientService.updateFolder(folder, accountId, folderId);
        call.enqueue(new Callback<Folder>() {
            @Override
            public void onResponse(Call<Folder> call, Response<Folder> response) {
                if (response.code() == 200){
                    finish();
                }else{
                    Toast.makeText(EditFolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Folder> call, Throwable t) {
                Toast.makeText(EditFolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce izvrsiti izmenu foldera", t);
            }
        });
    }
}
