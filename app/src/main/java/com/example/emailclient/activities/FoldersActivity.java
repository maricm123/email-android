package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.emailclient.R;
import com.example.emailclient.adapters.DrawerListAdapter;
import com.example.emailclient.adapters.FoldersAdapter;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.NavItem;
import com.example.emailclient.service.UtilsService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoldersActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private RelativeLayout drawerPane;
    private FoldersAdapter fAdapter;
    private ArrayList<Folder> folders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folders_wrapper);

        Toolbar toolbar = findViewById(R.id.toolbar_folders);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FoldersActivity.this, CreateFolderActivity.class);
                startActivity(intent);
            }
        });

        ListView folderList = findViewById(R.id.folder_list);
        fAdapter = new FoldersAdapter(FoldersActivity.this, folders);
        folderList.setOnItemClickListener(new FolderClickListener());
        folderList.setAdapter(fAdapter);

        drawerLayout = findViewById(R.id.drawerLayout);
        drawerList = findViewById(R.id.navList);
        drawerPane = findViewById(R.id.drawerPane);

        DrawerListAdapter adapter = new DrawerListAdapter(this, getNavItems());
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawerList.setAdapter(adapter);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        RelativeLayout relativeLayout = findViewById(R.id.profileBox);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoldersActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
    }


    private ArrayList<NavItem> getNavItems(){
        ArrayList<NavItem> navItems = new ArrayList<>();
        navItems.add(new NavItem(getString(R.string.emails), R.drawable.ic_drawer_mails));
        navItems.add(new NavItem(getString(R.string.contacts), R.drawable.ic_drawer_contacts));
        navItems.add(new NavItem(getString(R.string.folders), R.drawable.ic_drawer_folders));
        navItems.add(new NavItem(getString(R.string.settings), R.drawable.ic_drawer_settings));
        navItems.add(new NavItem(getString(R.string.logout), R.drawable.ic_drawer_logout));
        return navItems;
    }

    /* The click listner for ListView in the FoldersActivity content */
    private class FolderClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView name = view.findViewById(R.id.folder_name);
            Intent intent = new Intent(FoldersActivity.this, FolderActivity.class);
            intent.putExtra("folderId", id);
            intent.putExtra("name", name.getText());
            startActivity(intent);
        }
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        if(position == 0){
            Intent intent = new Intent(FoldersActivity.this, EmailsActivity.class);
            startActivity(intent);
        }else if(position == 1){
            Intent intent = new Intent(FoldersActivity.this, ContactsActivity.class);
            startActivity(intent);
        }else if(position == 2){
            Intent intent = new Intent(FoldersActivity.this, FoldersActivity.class);
            startActivity(intent);
        }else if(position == 3){
            Intent intent = new Intent(FoldersActivity.this, SettingsActivity.class);
            startActivity(intent);
        }else if(position == 4){

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(FoldersActivity.this, LoginActivity.class);
            startActivity(intent);
//            finish();
        }else{
            Log.e("DRAWER", "Nesto van opsega!");
        }

        drawerList.setItemChecked(position, true);
//        if(position != 5) // za sve osim za sync
//        {
//            setTitle(mNavItems.get(position).getmTitle());
//        }
        drawerLayout.closeDrawer(drawerPane);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int accountId = sharedPreferences.getInt("accountId", -1);

        Call <List<Folder>> call = UtilsService.emailClientService.getRootFolders(accountId);
        call.enqueue(new Callback<List<Folder>>() {
            @Override
            public void onResponse(Call<List<Folder>> call, Response<List<Folder>> response) {
                if (response.code() == 200){
                    folders.clear();
                    folders.addAll(response.body());
                    fAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(FoldersActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Folder>> call, Throwable t) {
                Toast.makeText(FoldersActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce preuzeti root-foldere", t);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
