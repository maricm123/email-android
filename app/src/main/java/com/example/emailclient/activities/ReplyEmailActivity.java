package com.example.emailclient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;
import com.example.emailclient.service.UtilsService;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReplyEmailActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_email);
        Toolbar toolbar = findViewById(R.id.toolbar_reply_message);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isValidSendToDraft()) {
                    Message message = prepareMessageToSendToDraft();
                    saveMessageToDrafts(message);
                    Toast.makeText(ReplyEmailActivity.this, "Message saved to drafts", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String emailAcc = sharedPreferences.getString("accountUserName", "");
        String displayNameAcc = sharedPreferences.getString("displayName", "");

        //ko odgovara na mail
        String fromText = displayNameAcc + "  <" + emailAcc + ">";
        TextView textView = findViewById(R.id.novi_email_posiljalac);
        textView.setText(fromText);

        //kome odgovaramo na mail
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String receiver = getIntent().getStringExtra("from");
        to_addressEdit.setText(receiver);

        //subject
        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = getIntent().getStringExtra("subject");
        subjectEdit.setText("Re: " + subject);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createmail_menuitem, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_send) {
            if(isValid()) {
                Message message = prepareMessageToSend();
                sendMessage(message);
            }
        }
        if(id == R.id.action_close_create) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isValidSendToDraft() {
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        if(TextUtils.isEmpty(to_address) && TextUtils.isEmpty(subject) && TextUtils.isEmpty(content) && TextUtils.isEmpty(cc) && TextUtils.isEmpty(bcc)) {
            return false;
        }else {
            return true;
        }
    }


    public Message prepareMessageToSendToDraft() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = contentEdit.getText().toString().trim();

        boolean unread = false;

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Message newMessage = new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, unread, folderId);

        return newMessage;
    }

    public void saveMessageToDrafts(Message message) {

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessageToDraft(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    finish();
                } else {
                    Toast.makeText(ReplyEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(ReplyEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    private boolean isValid(){
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();
        if (TextUtils.isEmpty(to_address)) {
            to_addressEdit.setError("Receiver is required");
            valid = false;
        }

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();
        if (TextUtils.isEmpty(subject)) {
            subjectEdit.setError("Subject is required");
            valid = false;
        }

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            contentEdit.setError("Content is required");
            valid = false;
        }

        return valid;
    }

    private Message prepareMessageToSend() {

        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = contentEdit.getText().toString().trim();

        boolean unread = false;

        Integer folderId = sharedPreferences.getInt("Sent", -1);

        return new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, unread, folderId);
    }

    private void sendMessage(Message message) {


        int folderId = sharedPreferences.getInt("Sent", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessage(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    Toast.makeText(ReplyEmailActivity.this, "Message sent", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(ReplyEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(ReplyEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
//        Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStop() {
        super.onStop();
//        Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStart() {
        super.onStart();
//        Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }
}