package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Attachment;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;
import com.example.emailclient.service.UtilsService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateEmailActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private ArrayList<Attachment> attachments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_email);
        Toolbar toolbar = findViewById(R.id.toolbar_create_email);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isValidSendToDraft()) {
                    Message message = prepareMessageToSendToDraft();
                    saveMessageInDraftsOnBackend(message);
                }

                finish();
            }
        });

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String emailAcc = sharedPreferences.getString("accountUserName", "");
        String displayNameAcc = sharedPreferences.getString("displayName", "");
        String fromText = displayNameAcc + "  <" + emailAcc + ">";
        TextView textView = findViewById(R.id.novi_email_posiljalac);
        textView.setText(fromText);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createmail_menuitem, menu);
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_send) {
            if(isValid()) {
                Message message = prepareMessageToSend();
                sendMessage(message);
                Toast.makeText(CreateEmailActivity.this, "Poruka je poslata", Toast.LENGTH_SHORT).show();
            }
        }
        if(id == R.id.action_close_create) {
            finish();
        }
        if(id == R.id.action_attachment) {
            Intent mRequestFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
            mRequestFileIntent.setType("*/*");
            startActivityForResult(mRequestFileIntent, 0);
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isValidSendToDraft() {
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        if(TextUtils.isEmpty(to_address) && TextUtils.isEmpty(subject) && TextUtils.isEmpty(content) && TextUtils.isEmpty(cc) && TextUtils.isEmpty(bcc)) {
            return false;
        }else {
            return true;
        }
    }

    public Message prepareMessageToSendToDraft() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = contentEdit.getText().toString().trim();

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Message newMessage = new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, false, folderId);

        return newMessage;
    }

    public void saveMessageInDraftsOnBackend(Message message) {

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessageToDraft(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    Toast.makeText(CreateEmailActivity.this, "Message saved to drafts", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(CreateEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(CreateEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    private boolean isValid(){
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();
        if (TextUtils.isEmpty(to_address)) {
            to_addressEdit.setError("Receiver is required");
//            to_addressEdit.requestFocus();
            valid = false;
        }

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();
        if (TextUtils.isEmpty(subject)) {
            subjectEdit.setError("Subject is required");
            valid = false;
        }

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();

        if (TextUtils.isEmpty(content)) {
            contentEdit.setError("Content is required");
            valid = false;
        }

        return valid;
    }

    private Message prepareMessageToSend() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = contentEdit.getText().toString().trim();

        Integer folderId = sharedPreferences.getInt("Sent", -1);

        Message newMessage = new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, false, folderId);
        newMessage.setAttachments(attachments);

        return newMessage;
    }

    private void sendMessage(Message message) {

        Integer folderId = sharedPreferences.getInt("Sent", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessage(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    finish();
                } else {
                    Toast.makeText(CreateEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(CreateEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();



    }


    @Override
    protected void onPause() {
        super.onPause();
//        Toast.makeText(this, "onPause()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStop() {
        super.onStop();
//        Toast.makeText(this, "onResume()", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onStart() {
        super.onStart();
//        Toast.makeText(this, "onStart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(this, "onRestart()", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent returnIntent) {
        super.onActivityResult(requestCode, resultCode, returnIntent);
        if (resultCode != RESULT_OK) {
            return;
        } else {
            Uri returnUri = returnIntent.getData();
            ParcelFileDescriptor mInputPFD = null;
            try {
                mInputPFD = getContentResolver().openFileDescriptor(returnUri, "r");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("MainActivity", "File not found.");
                return;
            }
            FileDescriptor fd = mInputPFD.getFileDescriptor();
            try {
                byte[] buffer = new byte[4096];
                ByteArrayOutputStream ous = new ByteArrayOutputStream();
                FileInputStream ios = new FileInputStream(fd);
                int read = 0;
                while ((read = ios.read(buffer)) != -1) {
                    ous.write(buffer, 0, read);
                }

                byte[] data = ous.toByteArray();
                Attachment attachment = new Attachment();
                attachment.setData(Base64.encodeToString(data, Base64.DEFAULT));
                attachment.setName(getNameFromUri(returnUri));
                attachment.setMime_type(getContentResolver().getType(returnUri));
                attachments.add(attachment);
                Toast.makeText(CreateEmailActivity.this,"Added file: " + attachment.getName() , Toast.LENGTH_LONG).show();
                ous.close();
            }catch (Exception e) {
                Toast.makeText(CreateEmailActivity.this, "Greska pri preuzimanju fajla", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getNameFromUri(Uri uri) {
        String fileName = "";
        if (uri.getScheme().equals("file")) {
            fileName = uri.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(uri, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));

                }
            } finally {

                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return fileName;
    }
}
