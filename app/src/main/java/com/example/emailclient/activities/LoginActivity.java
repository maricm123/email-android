package com.example.emailclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.emailclient.R;
import com.example.emailclient.model.Account;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.User;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.ProgressDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText inputUsername, inputPassword;
    private String username, password;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inputUsername = findViewById(R.id.input_username);
        inputPassword = findViewById(R.id.input_password);

        //set listeners
        inputUsername.addTextChangedListener(mTextWatcher);
        inputPassword.addTextChangedListener(mTextWatcher);

        //run once to disable if empty
        checkFieldsForEmptyValues();
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // check Fields For Empty Values
            checkFieldsForEmptyValues();
        }
    };

    void checkFieldsForEmptyValues(){
        loginButton = findViewById(R.id.btn_login);

        username = inputUsername.getText().toString();
        password = inputPassword.getText().toString();

        if(username.equals("")|| password.equals("")){
            loginButton.setEnabled(false);
        } else {
            loginButton.setEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,"Please wait...");
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.showDialog();
            User user = new User(username, password);

            Call<User> call = UtilsService.emailClientService.login(user);
            call.enqueue(new Callback<User>() {

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.code() == 200) {
                        storeUserData(response.body());
                        Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(LoginActivity.this, "Inserted data is not valid", Toast.LENGTH_SHORT).show();
                        inputUsername.getText().clear();
                        inputPassword.getText().clear();
                    }
                    progressDialog.dismissDialog();
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                    Log.v("ERROR", "Nije moguce ulogovati se", t);
                    progressDialog.dismissDialog();
                }
            });
            }
        });

    }

    private void storeUserData(User user) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putInt("userId", user.getId()).apply();

        Account account = user.getAccounts().get(0);
        for (Account acc: user.getAccounts()) {
            if (acc.getId() < account.getId()){
                account = acc;
            }
        }
        sharedPreferences.edit().putInt("accountId", account.getId()).apply();
        sharedPreferences.edit().putString("accountUserName", account.getUsername()).apply();
        sharedPreferences.edit().putString("displayName", account.getDisplayName()).apply();

        for (Folder folder: account.getFolders()) {
            sharedPreferences.edit().putInt(folder.getName(), folder.getId()).apply();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //nista ne radi
    }
}
