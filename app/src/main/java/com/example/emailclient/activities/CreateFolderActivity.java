package com.example.emailclient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.Rule;
import com.example.emailclient.service.UtilsService;
import com.example.emailclient.tools.RuleCustomView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateFolderActivity extends AppCompatActivity {

    private LinearLayout layoutForRules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar_create_folder);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layoutForRules = findViewById(R.id.rules_list);

        Button btnRule = findViewById(R.id.dodaj_rule);
        btnRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RuleCustomView viewRule = new RuleCustomView(getApplicationContext());
                layoutForRules.addView(viewRule);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.createfolder_manuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_savefolder){

            if (isValid()){
                Folder folder = prepareFolderForSaving();
                saveFolderOnBackend(folder);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValid(){
        boolean valid = true;

        EditText editFolderName = findViewById(R.id.edit_folder_name);
        String fName = editFolderName.getText().toString();
        if (("Inbox").equalsIgnoreCase(fName) || ("Sent").equalsIgnoreCase(fName)
                || ("Drafts").equalsIgnoreCase(fName)|| ("Trash").equalsIgnoreCase(fName)){
            editFolderName.setError("Reserved name");
            valid = false;
        }else if (TextUtils.isEmpty(fName)){
            editFolderName.setError("Missing value");
            valid = false;
        }

        int rulesCount = layoutForRules.getChildCount();
        for (int i = 0; i < rulesCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) layoutForRules.getChildAt(i);
            EditText editValue = relativeLayout.findViewById(R.id.edit_rule_value);
            String value = editValue.getText().toString();
            if(TextUtils.isEmpty(value)) {
                editValue.setError("Missing value");
                valid = false;
            }
        }
        return valid;
    }

    private Folder prepareFolderForSaving(){
        EditText editFolderName = findViewById(R.id.edit_folder_name);
        String folderName = editFolderName.getText().toString();

        ArrayList<Rule> rules = extractRulesFromDinamicLayout(layoutForRules);

        Folder noviFolder = new Folder();
        noviFolder.setName(folderName);
        int parentId = getIntent().getIntExtra("parentId", -1);
        if (parentId != -1){
            noviFolder.setParentId(parentId);
        }
        noviFolder.setRules(rules);
        return noviFolder;
    }

    private ArrayList<Rule> extractRulesFromDinamicLayout (LinearLayout layoutForRules){

        int rulesCount = layoutForRules.getChildCount();
        ArrayList<Rule> rules = new ArrayList<>();
        for (int i = 0; i < rulesCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) layoutForRules.getChildAt(i);

            Spinner spCondView = (Spinner) relativeLayout.findViewById(R.id.spinner_condition);
            String condition = spCondView.getSelectedItem().toString();

            EditText editValue = (EditText) relativeLayout.findViewById(R.id.edit_rule_value);
            String value = editValue.getText().toString();

            Spinner spOperView = (Spinner) relativeLayout.findViewById(R.id.spinner_operation);
            String operation = spOperView.getSelectedItem().toString();

            Rule rule = new Rule(condition, value, operation);
            rules.add(rule);
        }
        return rules;
    }

    private void saveFolderOnBackend(Folder folder){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int accountId = sharedPreferences.getInt("accountId", -1);

        Call<Folder> call = UtilsService.emailClientService.saveFolder(folder, accountId);
        call.enqueue(new Callback<Folder>() {
            @Override
            public void onResponse(Call<Folder> call, Response<Folder> response) {
                if (response.code() == 201){
                    finish();
                } else {
                    Toast.makeText(CreateFolderActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Folder> call, Throwable t) {
                Toast.makeText(CreateFolderActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce sacuvati folder", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
