package com.example.emailclient.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.emailclient.R;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;
import com.example.emailclient.service.UtilsService;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForwardEmailActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    String subject;
    EditText subjectEdit;
    String originalContent;
    EditText contentEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward_email);
        Toolbar toolbar = findViewById(R.id.toolbar_forward_message);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isValidSendToDraft()) {
                    Message message = prepareMessageToSendToDraft();
                    saveMessageToDrafts(message);
                    Toast.makeText(ForwardEmailActivity.this, "Message saved to drafts", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String emailAcc = sharedPreferences.getString("accountUserName", "");
        String displayNameAcc = sharedPreferences.getString("displayName", "");

        // Ko prosledjuje mail
        String fromText = displayNameAcc + "  <" + emailAcc + ">";
        TextView textView = findViewById(R.id.novi_email_posiljalac);
        textView.setText(fromText);

        // Subject
        subjectEdit = findViewById(R.id.novi_email_naslov);
        subject = getIntent().getStringExtra("subject");
        subjectEdit.setText("Fwd: " + subject);

        // Editabilan content (forward info + forward content)
        contentEdit = findViewById(R.id.novi_email_poruka);
        contentEdit.setText(createForwardedMessageText(fromText));

        // WebView prikaz originalnog contenta
        WebView view = findViewById(R.id.forwarded_email);
        originalContent = getIntent().getStringExtra("content");
        view.loadData(originalContent, "text/html", null);
    }

    //generise forward info
    private String createForwardedMessageText(String text){
        String sender = getIntent().getExtras().getString("from");
        return  "\n\n---Forwarded message---" +
                "\nFrom: " + sender +
                "\nDate: " + prepareMessageToSend().getDateTimeString() +
                "\nSubject: " + subject +
                "\nTo: " + text;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createmail_menuitem, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_send) {
            if(isValid()) {
                Message message = prepareMessageToSend();
                sendMessage(message);
            }
        }
        if(id == R.id.action_close_create) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isValidSendToDraft() {
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        if(TextUtils.isEmpty(to_address) && TextUtils.isEmpty(subject) && TextUtils.isEmpty(content) && TextUtils.isEmpty(cc) && TextUtils.isEmpty(bcc)) {
            return false;
        }else {
            return true;
        }
    }


    public Message prepareMessageToSendToDraft() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = contentEdit.getText().toString().trim();

        boolean unread = false;

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Message newMessage = new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, unread, folderId);

        return newMessage;
    }

    public void saveMessageToDrafts(Message message) {

        Integer folderId = sharedPreferences.getInt("Drafts", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessageToDraft(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    finish();
                } else {
                    Toast.makeText(ForwardEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(ForwardEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    private boolean isValid(){
        boolean valid = true;
        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString().trim();
        if (TextUtils.isEmpty(to_address)) {
            to_addressEdit.setError("Receiver is required");
            valid = false;
        }

        EditText subjectEdit = findViewById(R.id.novi_email_poruka);
        String subject = subjectEdit.getText().toString().trim();
        if (TextUtils.isEmpty(subject)) {
            subjectEdit.setError("Subject is required");
            valid = false;
        }

        EditText contentEdit = findViewById(R.id.novi_email_naslov);
        String content = contentEdit.getText().toString().trim();

        if (TextUtils.isEmpty(content)) {
            contentEdit.setError("Content is required");
            valid = false;
        }

        return valid;
    }

    private Message prepareMessageToSend() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String senderMail = sharedPreferences.getString("accountUserName", "");

        EditText to_addressEdit = findViewById(R.id.novi_email_primalac);
        String to_address = to_addressEdit.getText().toString();

        EditText ccEdit = findViewById(R.id.novi_email_cc);
        String cc = ccEdit.getText().toString();

        EditText bccEdit = findViewById(R.id.novi_email_bcc);
        String bcc = bccEdit.getText().toString();

        Date dateTime = new Date();
        String dateTimeString = DateConverter.toISO8601UTC(dateTime);

        EditText subjectEdit = findViewById(R.id.novi_email_naslov);
        String subject = subjectEdit.getText().toString().trim();

        EditText contentEdit = findViewById(R.id.novi_email_poruka);
        String content = replaceNewLineWithBreakLine(contentEdit.getText().toString().trim()) + "<br/><hr/><br/>" + this.originalContent;

        boolean unread = false;

        Integer folderId = sharedPreferences.getInt("Sent", -1);

        Message newMessage = new Message(senderMail, to_address, cc, bcc, dateTimeString, subject, content, unread, folderId);

        return newMessage;
    }

    private String replaceNewLineWithBreakLine(String text){
        return text.replace("\n", "<br/>");
    }

    private void sendMessage(Message message) {


        int folderId = sharedPreferences.getInt("Sent", -1);

        Call<Message> call = UtilsService.emailClientService.saveMessage(message, folderId);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 201){
                    Toast.makeText(ForwardEmailActivity.this, "Message is forwarded", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(ForwardEmailActivity.this, "Problem sa serverom:" + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(ForwardEmailActivity.this, "Problem pri komunikaciji sa serverom", Toast.LENGTH_SHORT).show();
                Log.v("ERROR", "Nije moguce poslati poruku", t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}