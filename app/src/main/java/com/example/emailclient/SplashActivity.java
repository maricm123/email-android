package com.example.emailclient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.example.emailclient.activities.EditEmailActivity;
import com.example.emailclient.activities.LoginActivity;

import java.util.Timer;
import java.util.TimerTask;


@SuppressLint("Registered")
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        if(MyConnectivityChecker.isConnected(this)){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            int SPLASH_TIME_OUT = 3000;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish(); // da nebi mogao da ode back na splash
                }
            }, SPLASH_TIME_OUT);
        }
        else{
            Toast.makeText(SplashActivity.this, "You are not connected to internet", Toast.LENGTH_SHORT).show();
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);
            int SPLASH_TIME_OUT = 3000;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish(); // da nebi mogao da ode back na splash
                }
            }, SPLASH_TIME_OUT);
        }
    }


    public static class MyConnectivityChecker {

        public static boolean isConnected(Context context){
            boolean connected = false;
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = connectivityManager .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            connected = (wifi.isAvailable() && wifi.isConnectedOrConnecting() || (mobile.isAvailable() && mobile.isConnectedOrConnecting()));
            return connected;
        }
    }
}
