package com.example.emailclient.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.emailclient.R;

public class ProgressDialog extends AlertDialog.Builder {

    private AlertDialog mDialog = null;

    public ProgressDialog(@NonNull Context context, String message) {
        super(context);
        final View container = LayoutInflater.from(context).inflate(R.layout.progress_dialog, null);
        setView(container);
        TextView textView = container.findViewById(R.id.progressMessage);
        textView.setText(message);
        mDialog = create();
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
    }

    public void showDialog() {
        if (mDialog != null && !mDialog.isShowing()) {
            mDialog.show();
        }
    }

    public void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
