package com.example.emailclient.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.emailclient.R;

public class RuleCustomView extends RelativeLayout {

    public RuleCustomView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addView(inflater.inflate(R.layout.include_rule, null));

        Spinner spinnerCondition = findViewById(R.id.spinner_condition);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                context, R.array.rule_condition, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCondition.setAdapter(adapter);

        Spinner spinnerOperation = findViewById(R.id.spinner_operation);
        ArrayAdapter<CharSequence> adapterOperation = ArrayAdapter.createFromResource(
                context, R.array.rule_operation, android.R.layout.simple_spinner_item);
//        adapterOperation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOperation.setAdapter(adapterOperation);

        Button deleteRule = (Button)findViewById(R.id.rule_delete);
        deleteRule.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout relLayout = (RelativeLayout) v.getParent();
                RuleCustomView customView = (RuleCustomView) relLayout.getParent();
                LinearLayout linearLayout = (LinearLayout) customView.getParent();
                linearLayout.removeView(customView);
            }
        });
    }

}
