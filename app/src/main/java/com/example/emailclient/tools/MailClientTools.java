package com.example.emailclient.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.preference.PreferenceManager;

import com.example.emailclient.R;
import com.example.emailclient.model.Message;
import com.example.emailclient.service.DateConverter;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MailClientTools {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static int calculateTimeTillNextSync(int minutes){
        return 1000 * 60 * minutes;
    }

    public static void sortirajPoruke(List<Message> poruke, Context context) {
        Collections.sort(poruke, new Comparator<Message>() {
            public int compare(Message m1, Message m2) {
                Date date1 = DateConverter.fromISO8601UTC(m1.getDateTimeString());
                Date date2 = DateConverter.fromISO8601UTC(m2.getDateTimeString());
                return date1.compareTo(date2);
            }
        });

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String sortOrder = sharedPreferences.getString(context.getString(R.string.sort_preference), "ASC");

        //sortiranje mailova po opadajucem redosledu - DESC
        if ("DESC".equals(sortOrder)) {
            Collections.reverse(poruke);
        }
    }

    public static void storeSyncPoint(List<Message> messages, Context context){

        int maxId = 0;
        for (Message mess: messages) {
            if (mess.getId()>maxId) {
                maxId = mess.getId();
            }
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int accountId = sharedPreferences.getInt("accountId", -1);
        int lastMaxId = sharedPreferences.getInt("maxId" + accountId, 0);
        if (maxId > lastMaxId){
            sharedPreferences.edit().putInt("maxId" + accountId, maxId).apply();
        }
    }

    public static String getShortString(String longString, int maxLength)
    {
        if(longString.length() <= maxLength)
        {
            return longString;
        }
        else
        {
            return longString.substring(0, maxLength-3)+"...";
        }
    }
}
