package com.example.emailclient.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UtilsService {

    /*
    s obzirom da svi mi imamo razlicite ip adrese, nakon sto pullujete, umesto <ipaddress> ubacite svoju ip adresu,
    ispred porta
    */
    public static final String SERVICE_API_PATH = "http://192.168.0.14:8080/api/";

    //za loging i debug
    public static OkHttpClient test(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();
    }


    //Modifikovan gson da zna pravilno transformisati ulazni datum (milisekunde) u Date objekat
    final static Gson gson = new GsonBuilder()
            .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                    return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                }
            })
            .create();

    //definisemo Retrofit instancu za dalju komunikaciju sa serverom
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(SERVICE_API_PATH)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(test())
            .build();


    //povezivanje sa interfejsom koji vrsi komunikaciju sa Mail_api
    public static EmailClientService emailClientService = retrofit.create(EmailClientService.class);

}
