package com.example.emailclient.service;

import com.example.emailclient.model.Attachment;
import com.example.emailclient.model.Contact;
import com.example.emailclient.model.Folder;
import com.example.emailclient.model.Message;
import com.example.emailclient.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*
 * Created by tasha on 11/06/2020
 */
public interface EmailClientService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @POST("users/login")
    Call<User> login(@Body User user);

    @GET("folders/{folderId}/messages")
    Call<List<Message>> getMessagesByFolder(@Path("folderId") int id);

    @GET("folders/{idFolder}/messages/{idMessage}")
    Call<Message> getMessage(@Path("idFolder") int idFolder,
                             @Path("idMessage") int idMessage);

    @GET("accounts/{idAcc}/folders")
    Call<List<Folder>> getRootFolders(@Path("idAcc") int accountId);

    @GET("accounts/{idAcc}/folders")
    Call<List<Folder>> getSubfolders(@Path("idAcc") int accountId,
                                     @Query("parentFolderId") int parentFolderId);

    @GET("accounts/{idAcc}/folders/{id}")
    Call<Folder> getFolderById(@Path("idAcc") int accountId,
                               @Path("id") int idFolder);

    @GET("users/{userId}/accounts/{idAcc}/messages")
    Call<List<Message>> getNewMessages(@Path("userId") int userId,
                                 @Path("idAcc") int accountId,
                                 @Query("maxId") int maxId);

    @POST("accounts/{idAcc}/folders")
    Call<Folder> saveFolder(@Body Folder folder,
                            @Path("idAcc") int accountId);

    @PUT("accounts/{idAcc}/folders/{id}")
    Call<Folder> updateFolder(@Body Folder folder,
                              @Path("idAcc") int accountId,
                              @Path("id") int folderId);

    @DELETE("accounts/{idAcc}/folders/{id}")
    Call<Void> deleteFolder(@Path("idAcc") int accountId, @Path("id") int folderId);

    @GET("users/{id}")
    Call<User> getUserById(@Path("id") int userId);

    @GET("users/{idUser}/contacts")
    Call<List<Contact>> getContactsByUser(@Path("idUser") int userId);


    @GET("users/{idUser}/contacts/{idContact}")
    Call<Contact> getContact(@Path("idUser") int userId,
                             @Path("idContact") int idContact);

    @POST("users/{idUser}/contacts")
    Call<Contact> saveContact(@Body Contact contact,
                              @Path("idUser") int idUser);

    @PUT("users/{idUser}/contacts/{idContact}")
    Call<Contact> updateContact(@Body Contact contact,
                                @Path("idUser") int idUser,
                                @Path("idContact") int idContact);

    @POST("folders/{idFolder}/messages")
    Call<Message> saveMessage(@Body Message message,
                              @Path("idFolder") int idFolder);

    @DELETE("users/{idUser}/contacts/{idContact}")
    Call<Void> deleteContact(@Path("idUser") int idUser,
                             @Path("idContact") int idContact);

    @POST("folders/{idFolder}/messages/drafts")
    Call<Message> saveMessageToDraft(@Body Message message,
                              @Path("idFolder") int idFolder);

    @PUT("folders/{idFolder}/messages/{id}")
    Call<Message> updateMessage(@Body Message message,
                                @Path("idFolder") int idFolder,
                                @Path("id") int idMess);

    @DELETE("folders/{idFolder}/messages/{id}")
    Call<Void> deleteMessage(@Path("idFolder") int idFolder,
                             @Path("id") int idMess);

    @GET("messages/{idMess}/attachments/{id}")
    Call<Attachment> getAttachment(@Path("idMess") int idMessage,
                                   @Path("id") int idAttachment);
}
